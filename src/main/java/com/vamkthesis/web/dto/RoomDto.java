package com.vamkthesis.web.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoomDto {
    private Long staffId;
    private String clientInfo;
    private String roomId;
    private Long clientId;
}
