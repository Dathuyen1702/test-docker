package com.vamkthesis.web.service;

import com.vamkthesis.web.dto.EmailSubcribeDto;

public interface IEmailSubcribeService {
    EmailSubcribeDto save(EmailSubcribeDto emailSubcribeDto);
}
